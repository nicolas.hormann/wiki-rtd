# El README de la wiki de UNC Supercómputo

![alt text](https://readthedocs.org/projects/ccad/badge/?version=latest "Build status")

## Setup

Es muy recomendable crear un virtual env:

```
python3 -m venv .env
. .env/bin/activate
pip install -r requirements.txt
```

## Run

Para correr la wiki localmente:

```
make clean livehtml
```

> Este comando también detecta cambios en el código y autorecompila automáticamente las páginas afectadas.


## Contrib

Este proyecto se organiza de la siguiente forma:

* `.readthedocs.yaml`: Configura el pipeline en ReadTheDocs.
* `Makefile`: Para compilar y correr `sphinx-autobuild`.
* `requirements.txt`: Dependencias.
* `docs/conf.py`: Configuración de Sphinx, como el tema, extensiones, etc.
* `docs/index.md`: Página inicial de la wiki, acá se define el menú lateral.
* `docs/empezar/`: Sección "Primeros pasos" del menú.
* `docs/infra/`: Sección "Infraestructura" del menú.
* `docs/tutoriales/`: Sección "Tutoriales" del menú.
* `docs/ayuda/`: Sección "Ayuda" del menú.
* `docs/_static/`: Archivos estáticos, como CSS o imágenes.
* `docs/_templates/`: Para cambiar secciones del HTML del tema.

### Agregar un tutorial

1. Simplemente crear un archivo en `docs/tutoriales/` con extensión `.md`.
1. Revisar otros archivos para entender las features extras que tiene el Markdown de Sphinx.

## Documentación

### Sphinx

https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html

### Markdown

https://myst-parser.readthedocs.io/en/v0.13.5/using/syntax-optional.html

https://mystmd.org/guide/code

### Tema

Nos basamos en Sphinix Book:
https://sphinx-book-theme.readthedocs.io/en/stable/

Que a su vez se basa en PyData Theme:
https://pydata-sphinx-theme.readthedocs.io/en/latest/user_guide/index.html

### Dependencias

Cross referencing:
https://docs.readthedocs.io/en/stable/guides/cross-referencing-with-sphinx.html

Resaltado de sintaxis de código:
https://pygments.org/docs/lexers/
