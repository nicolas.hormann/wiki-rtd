---
og:title: Software con licencia
---

# ™️ Software con licencia

No contamos con licencia de Matlab, Ansys, Gaussian, COSOL o VASP.

La instalación del servidor de licencias y del software deberá llevarse a cabo en el home del usuario y con su licencia.

Por otro lado, el usuario deberá ser responsable de que el servidor de licencias esté siempre ejecutando en la cabecera del cluster correspondiente,
e iniciarlo ante eventuales reinicios de la cabecera.

`````{admonition} Tip
:class: hint
Para que los nodos de cómputo se comuniquen con el nodo cabecera,
pueden hacerlo internamente a través de:

::::{tab-set}
:::{tab-item} Serafín
:sync: serafin
```{code-block}
head.rome.ccad.unc.edu.ar
```
:::
:::{tab-item} Mendieta
:sync: mendieta
```{code-block}
head.ivb.ccad.unc.edu.ar
```
:::
:::{tab-item} Eulogia
:sync: eulogia
```{code-block}
head.knl.ccad.unc.edu.ar
```
:::
:::{tab-item} Mulatona
:sync: mulatona
```{code-block}
head.bdw.ccad.unc.edu.ar
```
:::
::::

o directamente configurar la IP de la interfaz interna:

::::{tab-set}
:::{tab-item} Serafín
:sync: serafin
```{code-block}
10.6.10.250
```
:::
:::{tab-item} Mendieta
:sync: mendieta
```{code-block}
10.10.10.250
```
:::
:::{tab-item} Eulogia
:sync: eulogia
```{code-block}
10.2.10.250
```
:::
:::{tab-item} Mulatona
:sync: mulatona
```{code-block}
10.3.10.250
```
:::
::::

`````