---
og:title: Tmux
---

# 🪟 Tmux

Es posible que en algún momento necesite dejar un programa compilando durante varias horas.
En vez de mantener la sesión SSH activa, puede usar tmux para desconectarse sin interrumpir la compilación.
La herramienta ya está instalada en todos los clusters.

## Comandos

* Para crear una nueva sesión ejecute:
    ```console
    $ tmux
    ```

* Para desconectarse y que la sesión siga activa:
    {kbd}`Ctrl` + {kbd}`b`, {kbd}`d`

* Para reconectarse a la ultima sesión:
    ```console
    $ tmux a
    ```

* Para eliminar/cerrar la sesión:
    {kbd}`Ctrl` + {kbd}`d`

* Para ver las sesiones activas:
    ```console
    $ tmux ls
    ```

* Para conectarse a cierta sesión:
    ```console
    $ tmux attach -t 0
    ```

## Cheat sheets

Algunos cheat sheets más completos:

[https://tmuxcheatsheet.com/](https://tmuxcheatsheet.com/)

[https://duckduckgo.com/?q=tmux+cheat+sheet](https://duckduckgo.com/?q=tmux+cheat+sheet&ia=cheatsheet&iax=1)
