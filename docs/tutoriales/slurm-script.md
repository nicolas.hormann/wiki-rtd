---
og:title: Script de lanzamiento
---

# 🚀 Script de lanzamiento

Para lanzar un trabajo en SLURM, es necesario un *script de lanzamiento* 
que es un archivo que contiene:

- Lineas `#SBATCH` que describen los recursos que requiere un trabajo.
Cuando se encuentra una línea de la sección siguiente, el resto de
las líneas `#SBATCH` **son ignoradas**.

- El script para un shell UNIX (generalmente `bash`) propiamente
dicho, que enumera las tareas a ejecutar cuando el cluster corre el
trabajo.

## Preámbulo

### Hashbang

La primera línea del archivo siempre debe indicar el *shell* para el que
está escrito el script:

```slurm
#!/bin/bash
```

### Nombre

El trabajo debe tener un nombre, que le sirva para identificarlo.

```slurm
#SBATCH --job-name=nombre
```

### Notificaciones al usuario (opcional)

Se le puede pedir al manejador de recursos que notifique por email al
usuario sobre actualizaciones del estado de su trabajo.

```slurm
#SBATCH --mail-type=ALL    
#SBATCH --mail-user=direccion@de.email
```

## Recursos

Luego se deben enumerar los recursos requeridos por el trabajo.

(slurm-script-cola)=
### Cola de ejecución

Los distintos recursos de cómputo de los clusters del CCAD son
accesibles a través de **colas de trabajos**, que son líneas de espera
en las que trabajos con requerimientos similares se ubican hasta que les
toque su turno y los recursos requeridos estén disponibles.
Las colas disponibles y sus límites [dependen de cada cluster](infra-slurm-colas).

Para seleccionar la cola `short`:

```slurm
#SBATCH --partition=short
```

### Procesos (tasks)

Cada trabajo en ejecución consiste en una cantidad de procesos corriendo
simultáneamente.

- Los procesos se distribuyen en uno o más *nodos* de cómputo
(computadoras del cluster) según la disponibilidad de recursos.

- En las aplicaciones **MPI**, la cantidad de procesos es el parámetro
`-np` pasado a `mpirun` o `mpiexec`.

- En el script, la cantidad **P** de procesos del trabajo se define
con la línea:

    ```slurm
    #SBATCH --ntasks=P
    ```

- Alternativamente se puede pedir una distribución exacta y equitativa
de procesos entre nodos del cluster, especificando explícitamente la
cantidad de nodos *N* y cantidad de procesos por nodo *PPN*:

    ```slurm
    #SBATCH --nodes=N
    #SBATCH --ntasks-per-node=PPN
    ```

### Hilos (cpus, opcional)

Cada proceso del trabajo puede dividirse en más de un *hilo* de ejecución. 

A cada hilo del trabajo se le asigna un *core* de algún procesador del
cluster. En el script, la cantidad **H** de hilos por proceso se
define con la línea:

```slurm
#SBATCH --cpus-per-task=H
```

Entonces, si un trabajo pide **P** procesos con **H** hilos cada uno,
requerirá **P \* H** cores para correr.

```{note}
En las aplicaciones **OpenMP**, la cantidad de hilos por
proceso está definida en la variable de entorno `OMP_NUM_THREADS`.
```

### Tiempo

Es necesario especificar el tiempo máximo de ejecución del trabajo. Si
el trabajo se excede de este tiempo, el manejador de recursos mata los
procesos en ejecución y cancela el trabajo. Este tiempo debería ser lo
más cercano posible al tiempo de ejecución real del trabajo. El formato
del campo es `días-horas:minutos`.

```slurm
#SBATCH --time=dd-hh:mm
```

### Aceleradores (opcional)

Algunos de los clusters del CCAD disponen de coprocesadores para cómputo
(GPUs, Xeon Phi, etc.). Los aceleradores se piden **por nodo**, y son
visibles a todos los procesos corriendo dentro del mismo nodo. Para
pedir **G** GPUs por nodo:

```slurm
#SBATCH --gres=gpu:G
```

### Reserva (opcional)

Si tiene una reserva asignada, puede usarla con el siguiente línea:

```slurm
#SBATCH --reservation=R
```

### Job Array (opcional)

Puede generar múltiples jobs casi idénticos usando [Job Array](https://slurm.schedmd.com/job_array.html).

Puede ser útil en las siguientes situaciones:

1. Ejecutar el mismo programa de análisis varias veces con diferentes archivos o conjuntos de datos.
1. Ejecutar el mismo programa con distintos argumentos.
1. Repetir varias veces la ejecución de un programa.

```slurm
#SBATCH --array=1-3
```

:::{dropdown} Ejemplo de variables
Para cada job, se definen las siguientes variables:
```bash
# 1
SLURM_JOB_ID=36
SLURM_ARRAY_JOB_ID=36
SLURM_ARRAY_TASK_ID=1
SLURM_ARRAY_TASK_COUNT=3
SLURM_ARRAY_TASK_MAX=3
SLURM_ARRAY_TASK_MIN=1

# 2
SLURM_JOB_ID=37
SLURM_ARRAY_JOB_ID=36
SLURM_ARRAY_TASK_ID=2
SLURM_ARRAY_TASK_COUNT=3
SLURM_ARRAY_TASK_MAX=3
SLURM_ARRAY_TASK_MIN=1

# 3
SLURM_JOB_ID=38
SLURM_ARRAY_JOB_ID=36
SLURM_ARRAY_TASK_ID=3
SLURM_ARRAY_TASK_COUNT=3
SLURM_ARRAY_TASK_MAX=3
SLURM_ARRAY_TASK_MIN=1
```
:::

## Script

### Entorno

Primero es necesario importar la configuración de las variables de
entorno compartida por todos los usuarios:

```bash
. /etc/profile
```

### Módulos

Una vez cargado el entorno por defecto tenemos acceso al sistema de
[módulos](/tutoriales/modules), por lo que cargamos los requeridos por el programa
a correr:

```bash
module load modulo/del/programa
```

### Variables de entorno (opcional)

Luego podemos configurar variables de entorno que afecten el
funcionamiento de nuestro programa.

Un caso común es el de las aplicaciones OpenMP, que deben configurar la
variable `OMP_NUM_THREADS` al mismo valor que fue dado en [`#SBATCH
--cpus-per-task`](#hilos-cpus-opcional).

```bash
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
```

### Lanzamiento del programa

Finalmente se lanza el programa. Para esto se debe utilizar
[`srun`](http://slurm.schedmd.com/srun.html), que suplanta la
funcionalidad de `mpirun` o `mpiexec` lanzando tantos procesos como sean
necesarios en los recursos asignados al trabajo.

```bash
srun programa
```