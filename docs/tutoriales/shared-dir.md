---
og:title: Directorios compartidos
---

# 🗂️ Directorios compartidos

Si se necesita compartir un directorio entre dos o más usuarios dentro de un mismo cluster,
podemos crear un grupo con todos los usuarios junto con un directorio compartido en `/home/shared/<grupo>`.

Para solicitarlo, envíe un email a [soporte](soporte), usando la siguiente plantilla:

```{code-block}
:caption: Solicitud de grupo de usuarios
Hola soporte,

Me gustaría solicitar la creación de un grupo de usuarios.

Clusters: <nombre de los clusters>
Nombre: <nombre sugerido para el grupo>
Usuarios: <usernames integrantes del grupo>
Notas adicionales: <alguna información extra>

Desde ya muchas gracias!
```
