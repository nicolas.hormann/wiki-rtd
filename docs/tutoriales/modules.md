---
og:title: Software preinstalado
---

# 🧰 Software preinstalado

El acceso a los programas instalados en los clusters se realiza a través
de [Environment Modules](http://modules.sourceforge.net/).

El comando `module` se utiliza para cargar y gestionar módulos de entorno.
Los módulos son conjuntos de variables de entorno y configuraciones
que permiten a los usuarios cambiar fácilmente entre diferentes
entornos de software sin tener que configurar manualmente cada variable.

## Resumen de comandos

:module av: Ver los módulos disponibles
:module load: Cargar un módulo
:module list: Ver una lista de módulos cargados
:module purge: Quitar todos los módulos cargados

## Ejemplo: Cargar compiladores Fortran

%TODO: cargar tambien el modulo openmpi

:::{asciinema} yNc2DuCGJgN6gB3AqKtFS7kro
:::

## Preguntas frecuentes

:::{dropdown} ¿Qué flags de compilación utilizo para mejor performance?
:name: hola
El módulo de cada compilador incluye flags de compilación adecuados
(aunque conservadores) para dicho compilador. Estos se pueden ver
corriendo `"env | grep FLAGS"` después de cargar el módulo
correspondiente.
:::

:::{dropdown} ¡No veo ninguna biblioteca!
Es necesario cargar el módulo del compilador a utilizar para poder ver
las bibliotecas disponibles para dicho compilador.
:::

:::{dropdown} La biblioteca o aplicación que necesito no está disponible, ¿qué hago?
Comunicarse con [soporte](soporte) para que la instalemos.
:::

:::{dropdown} ¿Por qué no instalaron una versión más nueva de tal herramienta?
Las versiones elegidas son las más nuevas posibles al momento de la
instalación del software del cluster. Agregar versiones de compiladores
multiplica la cantidad de bibliotecas a compilar y para las que dar
soporte, por lo que sólo se hace de ser extremadamente necesario.
:::

:::{dropdown} ¿Por qué no instalaron Clang en vez de GCC o ICC?
Porque la suite de compiladores de LLVM no tiene un compilador Fortran
estable todavía. Estamos siguiendo el progreso de Flang.
:::

:::{dropdown} ¿Por qué no eligieron MVAPICH2 o Intel MPI como MPI?
Porque queremos un único MPI para todos los clusters, y algunas
aplicaciones no los soportan.
:::

:::{dropdown} Necesito otra versión de los compiladores disponibles para mi aplicación. ¿Qué puedo hacer?
Los usuarios pueden compilar e instalar toolchains propias en sus homes.
Recomendamos utilizar [Spack](/tutoriales/spack) para estas situaciones.
:::

:::{dropdown} ¡Necesito Python con un conjunto de módulos particulares y el Python instalado es muy viejo!
Actualmente no tenemos una solución genérica a este problema que
satisfaga a la mayor cantidad posible de usuarios. Recomendamos que los
usuarios instalen su propio intérprete Python con los paquetes que
requieran en sus propios homes utilizando herramientas como
[Miniconda](https://conda.io/miniconda.html).
:::
