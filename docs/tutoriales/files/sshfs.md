# SSHFS

SSHFS (Secure SHell FileSystem) es una herramienta
que permite montar un sistema de archivos remoto mediante SSH (Secure SHell),
de manera que los archivos remotos se pueden acceder y manipular como si estuvieran en el sistema local.

En primer lugar se debe crear en el sistema cliente un directorio que
utilizaremos a continuación como punto de montaje local:

```console
$ mkdir $HOME/datos_mendieta
```

Luego se realiza el montaje reemplazando la variable $USUARIO por el nombre
de usuario asignado en el cluster:

```console
$ sshfs $USUARIO@mendieta.ccad.unc.edu.ar:/home/$USUARIO $HOME/datos_mendieta
```

Comprobar que el directorio se ha montado correctamente y que se poseen
los permisos para operar en el sistema remoto:

```console
$ ls $HOME/datos_mendieta
$ touch $HOME/datos_mendieta/puedoescribir
$ rm $HOME/datos_mendieta/puedoescribir
```

Para desmontar el directorio se utiliza el siguiente comando:

```bash
fusermount -u $HOME/datos_mendieta
```

:::{tip}
Para hacer esto permanente, ver [FSTAB](fstab).
:::