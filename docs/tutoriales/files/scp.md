
# SCP

SCP (Secure Copy Protocol) es un método para transferir archivos
de manera segura entre un sistema localy un sistema remoto, o entre dos sistemas remotos,
utilizando el protocolo SSH (Secure Shell) para la autenticación y la seguridad de la transferencia.


Comando básico:
``` console
$ scp [opciones] origen destino
```

:::{tip}
Para una herramienta más avanzada, ver [Rsync](rsync).
:::

## Subir archivo
Subir el archivo `file.csv` al cluster:

::::{tab-set}
:::{tab-item} Serafín
```{code-block} console
$ scp file.csv $USUARIO@serafin.ccad.unc.edu.ar:~/destino/
```

:::
:::{tab-item} Mendieta
```{code-block} console
$ scp file.csv $USUARIO@mendieta.ccad.unc.edu.ar:~/destino/
```
:::
:::{tab-item} Eulogia
```{code-block} console
$ scp file.csv $USUARIO@eulogia.ccad.unc.edu.ar:~/destino/
```
:::
:::{tab-item} Mulatona
```{code-block} console
$ scp file.csv $USUARIO@mulatona.ccad.unc.edu.ar:~/destino/
```
:::
::::

## Bajar archivo
Baja el archivo `$HOME/file.csv` a la computadora del usuario:

::::{tab-set}
:::{tab-item} Serafín
```{code-block} console
$ scp $USUARIO@serafin.ccad.unc.edu.ar:~/file.csv .
```

:::
:::{tab-item} Mendieta
```{code-block} console
$ scp $USUARIO@mendieta.ccad.unc.edu.ar:~/file.csv .
```
:::
:::{tab-item} Eulogia
```{code-block} console
$ scp $USUARIO@eulogia.ccad.unc.edu.ar:~/file.csv .
```
:::
:::{tab-item} Mulatona
```{code-block} console
$ scp $USUARIO@mulatona.ccad.unc.edu.ar:~/file.csv .
```
:::
::::

## Copiar un archivo entre dos clusters

```{code-block} console
$ scp $USUARIO@serafin.ccad.unc.edu.ar:~/origen.csv $USUARIO@mendieta.ccad.unc.edu.ar:~/destino.csv
```

## Copiar un directorio

Para transferir directorios de forma recursiva, agregar el parametro `-r`.

```{code-block} console
$ scp -r origen/ $USUARIO@serafin.ccad.unc.edu.ar:~/destino/
```