---
og:title: Gestión de archivos
---

# 📝 Gestión de archivos

:::{toctree}
:hidden:
mobaxterm
de
sshfs
fstab
scp
rsync
:::

Existe una variedad de herramientas para subir, descargar,
y administrar los archivos (copiar, renombrar, editar, etc.) en los clusters.

Algunas herramientas permiten copiar un archivo o directorio desde o hacia el cluster.

Otras permiten _montar_ localmente el directorio `/home/$USUARIO` del cluster
y de este modo realizar todas las operaciones sobre los archivos
(copiar, mover, renombrar, editar, etc.)
con las herramientas de preferencia instaladas en su PC de trabajo.

```{warning}
Todos estos métodos funcionarán únicamente si las llaves SSH están correctamente configuradas.
```

## Tutoriales

* [MobaXterm (SFTP)](mobaxterm): Si usa Windows.
* [GNOME/KDE](de): Para usuarios con esos entornos de escritorio.
* [Secure Shell Filesystem (SSHFS)](sshfs): Permite montar el home dentro de un directorio local.
* [File Systems TABle (FSTAB)](fstab): Hace que la opción anterior se ejecute en el arranque.
* [Secure Copy Protocol (SCP)](scp): Copiar un archivo o directorio.
* [Remote SYNC (Rsync)](rsync): Copiar un directorio con opciones más avanzandas.

Para decidir que método usar, puede ayudarse con el siguiente árbol de decisiones:

```{raw} html
:file: ../../_static/tutoriales/files/tree.svg
```
