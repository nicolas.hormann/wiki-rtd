# Rsync

Rsync (Remote SYNC) es una herramienta de software libre
para la sincronización de archivos y directorios entre diferentes ubicaciones de manera eficiente.
Utiliza un algoritmo de delta para minimizar la cantidad de datos transferidos,
lo que lo hace rápido y adecuado para copias de seguridad y replicación de datos.

Comando básico:
```console
$ rsync -avz origen destino
```

* `-a` es para el modo archivo (preserva atributos).
* `-v` para modo detallado (verbose).
* `-z` para comprimir los datos durante la transferencia.

:::{tip}
Para una herramienta más simple, ver [SCP](scp).
:::

## Subir archivos

Subir el directorio `ruta/origen` al cluster:

::::{tab-set}
:::{tab-item} Serafín
```{code-block} console
$ rsync -avz ruta/origen $USUARIO@serafin.ccad.unc.edu.ar:~/ruta/destino/
```
:::
:::{tab-item} Mendieta
```{code-block} console
$ rsync -avz ruta/origen $USUARIO@mendieta.ccad.unc.edu.ar:~/ruta/destino/
```
:::
:::{tab-item} Eulogia
```{code-block} console
$ rsync -avz ruta/origen $USUARIO@eulogia.ccad.unc.edu.ar:~/ruta/destino/
```
:::
:::{tab-item} Mulatona
```{code-block} console
$ rsync -avz ruta/origen $USUARIO@mulatona.ccad.unc.edu.ar:~/ruta/destino/
```
:::
::::

## Bajar archivos

::::{tab-set}
:::{tab-item} Serafín
```{code-block} console
$ rsync -avz $USUARIO@serafin.ccad.unc.edu.ar:~/ruta/origen/ ruta/destino/
```
:::
:::{tab-item} Mendieta
```{code-block} console
$ rsync -avz $USUARIO@mendieta.ccad.unc.edu.ar:~/ruta/origen/ ruta/destino/
```
:::
:::{tab-item} Eulogia
```{code-block} console
$ rsync -avz $USUARIO@eulogia.ccad.unc.edu.ar:~/ruta/origen/ ruta/destino/
```
:::
:::{tab-item} Mulatona
```{code-block} console
$ rsync -avz $USUARIO@mulatona.ccad.unc.edu.ar:~/ruta/origen/ ruta/destino/
```
:::
::::

## Copiar archivos entre dos clusters

```{code-block} console
$ rsync -avz $USUARIO@serafin.ccad.unc.edu.ar:~/ruta/origen/ $USUARIO@mendieta.ccad.unc.edu.ar:~/ruta/destino/
```