# GNOME/KDE

Los usuarios de GNOME o KDE pueden _montar_ el directorio
home del cluster directamente desde la interfaz gráfica.

## GNOME (Nautilus)

1. Abrir un gestor de archivos

1. Hacer click en {menuselection}`+ Otras ubicaciones --> Conectarse al servidor`

1. En el campo poner:

    ::::{tab-set}
    :::{tab-item} Serafín
    ```{code-block}
    ssh://serafin.ccad.unc.edu.ar
    ```
    :::
    :::{tab-item} Mendieta
    ```{code-block}
    ssh://mendieta.ccad.unc.edu.ar
    ```
    :::
    :::{tab-item} Eulogia
    ```{code-block}
    ssh://eulogia.ccad.unc.edu.ar
    ```
    :::
    :::{tab-item} Mulatona
    ```{code-block}
    ssh://mulatona.ccad.unc.edu.ar
    ```
    :::
    ::::

1. Hacer clic en {guilabel}`Conectar`.

1. En el cuadro de dialogo completar el usuario, dejando en blanco la contraseña, y hacer clic en {guilabel}`Conectar`.

```{tip}
Para que quede configurada la conexión, puede hacer {menuselection}`Clic secundario --> Añadir marcador`.
```

% TODO: KDE, screenshots?