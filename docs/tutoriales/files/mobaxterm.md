# MobaXterm

MobaXterm es una aplicación todo en uno que ofrece un entorno avanzado de terminal para Windows.
Entre sus funcionalidades, se encuentra SFTP/FTP (Secure File Transfer Protocol),
que proporciona transferencia de archivos segura y rápida entre el cliente y el servidor remoto.

1. El primer paso es iniciar MobaXterm, presionamos en "Settings" y luego "Configuration".
```{figure} /_static/tutoriales/files/mobaxterm/upload_win_1.png
```

2. Hacemos clic en la pestaña "SSH" y marcamos la opción que dice _Use internal SSH agent "MobaAgent"_. El programa le solicitará reiniciarse para aplicar los cambios.
```{figure} /_static/tutoriales/files/mobaxterm/upload_win_2.png
```

3. Una vez reiniciado, volvemos a la pestaña de configuración y veremos lo siguiente:
```{figure} /_static/tutoriales/files/mobaxterm/upload_win_3.png
```
Nota: Recuerde usar la versión instalable de MobaXterm. La versión portable da problemas con las claves SSH.

4. Ya tenemos lista la configuración SSH, procedemos a configurar la sesión SFTP, para ello, primero hacemos clic en "Sessions" y luego en "User sessions".
```{figure} /_static/tutoriales/files/mobaxterm/upload_win_4.png
```

5. Seleccionamos "SFTP".
```{figure} /_static/tutoriales/files/mobaxterm/upload_win_5.png
```

6. Procedemos a configurar el "Username" y el "Host".
```{figure} /_static/tutoriales/files/mobaxterm/upload_win_6.png
```
+ 🔵 Username: Nombre de usuario asignado.
+ 🔴 Remote host: _cluster_.ccad.unc.edu.ar (Reemplazamos cluster con el correspondiente).

Ejemplo:
+ 🔵 Username: napellido
+ 🔴 Remote host: serafin.ccad.unc.edu.ar

Por último, presionamos "Ok" para iniciar la sesión SFTP.

7. Si se tuvo éxito, veremos lo siguiente:
```{figure} /_static/tutoriales/files/mobaxterm/upload_win_7.png
```
El rectángulo marcado en azul corresponde a los archivos locales (Nuestra PC), mientras que el rectángulo rojo son los archivos presentes en el cluster.

Para intercambiar archivos entre uno y otro, solo basta con arrastrar los archivos requeridos del recuadro de la computadora correspondiente hacia el otro.

8. Para iniciar nuevamente la sesión SFTP, al abrir MobaXterm, veremos que se han guardado las credenciales de acceso y solo necesitamos hacer clic sobre la sesión.
```{figure} /_static/tutoriales/files/mobaxterm/upload_win_8.png
```
