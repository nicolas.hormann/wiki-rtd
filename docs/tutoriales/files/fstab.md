# FSTAB

FSTAB (File Systems TABle) es un archivo de configuración en sistemas operativos Linux
que especifica cómo los sistemas de archivos, dispositivos de almacenamiento y otras particiones
deben ser montados y utilizados por el sistema al arrancar.

:::{tip}
Para montar el home del cluster de forma temporal, ver [SSHFS](sshfs).
:::

Es posible configurar el sistema cliente para que monte automáticamente
el directorio home del cluster al arranque.
Para ello debemos seguir algunos pasos muy sencillos.
En primer lugar asegurarse que el usuario
pertenece al grupo `fuse` o bien que puede acceder en lectura al fichero `/etc/fuse.conf`.
El fichero `/etc/fuse.conf` debe contener la siguiente instrucción:

```{code-block}
:caption: /etc/fuse.conf
user_allow_other
```
Si no es el caso, editar el fichero con el superusuario root.

Una vez hecho esto, agregar la siguiente linea al fichero `/etc/fstab`:

```{code-block}
:caption: /etc/fstab
$USUARIO@mendieta.ccad.unc.edu.ar:/home/$USUARIO  $MOUNTPOINT fuse.sshfs auto,_netdev,user,idmap=user,transform_symlinks,identityfile=$HOME/.ssh/id_rsa,allow_other,default_permissions, 0 0
```

El reemplazo de las variables se hará de esta manera:

* `$USUARIO`: es el nombre de usuario en los clusters.
* `$MOUNTPOINT`: es el nombre del directorio local donde se quieren montar los datos remotos.
    Debe contener el path absoluto, por ejemplo `/home/maradona/datos_mendieta`.
* `$HOME`: es el directorio home del usuario local donde se encuentra la llave SSH privada.
