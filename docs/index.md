---
html_theme.sidebar_secondary.remove:
og:title: La wiki del CCAD
myst:
    html_meta:
        google-site-verification: "7Da_Hl2BTRzPZpgbVz_rwIK7SBooHsT3hklGplpiWEA"
---

# La wiki del CCAD 🧉

Información útil para trabajar con nuestros clusters.

:::{admonition} Recibimos sugerencias!
:class: hint
Si encuentra algún error, falta u omisión, o cree que estaría bueno agregar algún contenido a esta wiki,
no dude en [enviarnos sugerencias](sugerencias).
:::

## Primeros pasos

Si nunca usó el cluster o tiene poca experiencia en Linux,
en esta sección encontrará información sobre cómo arrancar.

:::{toctree}
:caption: Primeros pasos
:maxdepth: 1

empezar/desde-cero
empezar/abrir-cuenta
empezar/conexion
empezar/slurm
:::

## Infraestructura

En esta sección encontrará información sobre cada cluster disponible.

:::{toctree}
:caption: Infraestructura
:titlesonly:
:maxdepth: 2

infra/clusters/index
infra/almacenamiento
:::

## Tutoriales

Tutoriales sobre temas o tareas específicas.

:::{toctree}
:caption: Tutoriales
:maxdepth: 1

tutoriales/ssh
tutoriales/files/index
tutoriales/slurm-script
tutoriales/modules
tutoriales/licenses
tutoriales/spack
tutoriales/tmux
tutoriales/shared-dir
tutoriales/parallel
:::


## Ayuda

¿No encontró lo que buscaba?

:::{toctree}
:caption: Ayuda
ayuda/soporte
ayuda/colaborar
:::

:::{toctree}
:hidden:
:::
