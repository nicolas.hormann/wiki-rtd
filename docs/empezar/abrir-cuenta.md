---
html_theme.sidebar_secondary.remove:
og:title: Solicitar una cuenta
---

# 🪪 Solicitar una cuenta

Para acceder a los recursos computacionales del CCAD
es necesario tener o [generar un par de llaves SSH](/tutoriales/ssh).

Luego deberá solicitar una cuenta completando el siguiente
[formulario](https://ccad.unc.edu.ar/servicios/pedido-de-cuentas/).

Una vez enviado el formulario, le llegará un email con su usuario
e [instrucciones para conectarse](conexion)
a los distintos [clusters disponibles](/infra/clusters/index).

Una vez que haya podido ingresar a alguno de los clusters,
puede usar [software preinstalado](/tutoriales/modules) por los administradores,
o [compilar sus propias aplicaciones](/tutoriales/spack) en su home.

Por último, debe [usar el gestor de trabajos](slurm) para ejecutar sus programas de manera óptima.
