---
og:title: Conexión al cluster
---

# 🔒 Conexión al cluster

En esta página encontrará información sobre como conectarse a los clusters desde su PC.


## Nodo cabecera

Recuerde que un cluster se compone de muchas computadoras interconectadas, que las llamamos _nodos_.

Cuando se conecta a un cluster, en realidad se está conectando a un nodo especial, llamado _nodo cabecera_.
Este nodo **NO** se usa para realizar calculos, sinó que para ello están los _nodos de cómputo_.
Para hacer uso de estos nodos, se usa un [gestor de trabajos](slurm).

## Secure SHell

Para poder acceder al cluster debe contar con un **cliente ssh**. Casi
la totalidad de las distribuciones LINUX ya lo tienen preinstalado. Si
éste es su caso pude ejecutar el siguiente comando desde una terminal:

```console
$ ssh $USUARIO@serafin.ccad.unc.edu.ar
```

o bien

```console
$ ssh $USUARIO@mendieta.ccad.unc.edu.ar
$ ssh $USUARIO@eulogia.ccad.unc.edu.ar
$ ssh $USUARIO@mulatona.ccad.unc.edu.ar
```

En caso que quisiera iniciar lo conexión desde una maquina WINDOWS
recomendamos el programa
[MoabXterm](https://mobaxterm.mobatek.net/download.html).

La variable `$USUARIO` ha sido comunicada en el mail de confirmación de
creación de la cuenta y será su nombre de usuario dentro de los
clusters.

```{admonition} Sin contraseña
:class: important
Para conectarse a la cabecera del cluster no es necesario especificar
una contraseña pues la llave SSH pública enviada en la solicitud
se usa como método de autenticación.
```

```{seealso}
Para más información, [ver el tutorial sobre SSH](/tutoriales/ssh).
```

## No logro conectarme al cluster

```{rubric} ¿Qué puedo hacer antes de contactar el soporte?
```

1. Asegúrese que la clave privada corresponda con la clave pública que usted compartió con el CCAD:

    ```bash
    # el resultado debe coincidir con la pública, e.g. (~/.ssh/id_rsa.pub)
    ssh-keygen -y -f $HOME/.ssh/id_rsa
    ```    

1. Asegúrese que los permisos del fichero que contiene la llave privada
son correctos ejecutando el siguiente comando:

    ```bash
    chmod 600 $HOME/.ssh/id_rsa
    ```

1. Asegúrese que los permisos de la carpeta que contiene la llave
privada son correctos ejecutando el siguiente comando:

    ```bash
    chmod 700 $HOME/.ssh
    ```

1. Intente conectarse al cluster forzando el uso de la llave privada:

    ```bash
    ssh -i  $HOME/.ssh/id_rsa $USUARIO@mendieta.ccad.unc.edu.ar
    ```

1. Si aun así no funciona, pruebe nuevamente con el siguiente comando:

    ```bash
    SSH_AUTH_SOCK=0 ssh -i  $HOME/.ssh/id_rsa $USUARIO@mendieta.ccad.unc.edu.ar
    ```

    Si con este comando la conexión funciona, debe simplemente agregar la
    llave privada al repositorio del agente de autenticación:

    ```bash
    ssh-add
    ```

```{rubric} La conexión al cluster funciona forzando el uso de la llave SSH con ssh -i
```

Si la conexión funciona forzando la utilización de la llave SSH,
significa que el problema no se encuentra en el cluster sino en el
cliente. En la mayoría de los casos este problema se soluciona
modificando el fichero `/etc/ssh/sshd_config` presente en el sistema de
su PC. Debe buscar en el mismo la directiva IdentityFile y modificarla
de la siguiente manera:

```
IdentityFile ~/.ssh/id_rsa
```

```{rubric} Intenté todo pero sigue fallando
```

Es momento de contactar al [soporte](soporte), al hacerlo por favor adjunte la
salida del comando

```bash
ssh -vvv $USUARIO@mendieta.ccad.unc.edu.ar
```