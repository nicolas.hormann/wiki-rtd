---
og:title: Desde cero
---

# 🐣 Desde cero

Si nunca usó un cluster o tiene poca experiencia en Linux, en esta sección encontrará recursos para iniciarse.

## Desde 0 al CCAD
Le recomendamos leer
[esta presentación](https://docs.google.com/presentation/d/e/2PACX-1vRJZN8GUK6rOSXuU27h4pBE2xLpQazTS3XGJ_KhJW2H2_qbQma5nBdpdp1Cm938pA/pub?start=false&loop=false&delayms=3000),
también disponible en versión video, dictada en el marco de las
«Friends of Friends Hybrid Meeting 2022» organizado por el
Observatorio Astronómico de Córdoba (OAC, UNC).

```{youtube} hZJjOQKmElg
:width: 100%
```

---

## The Missing Semester

También recomendamos [The Missing Semester of Your CS Education](https://missing.csail.mit.edu/) del MIT CSAIL
en inglés y con una [traducción incompleta al español](https://missing-semester-esp.github.io/).