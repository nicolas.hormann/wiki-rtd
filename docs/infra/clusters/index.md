---
html_theme.sidebar_secondary.remove:
og:title: Clusters disponibles
og:description: Resumen y comparación de las caracteristicas de cada cluster disponible
---
# ⚡ Clusters disponibles

% TODO: en algún momento armar una página para cada cluster, con fotitos y no se que más
%```{toctree}
%:caption: Clusters
%:hidden:
%serafin
%mendieta
%eulogia
%mulatona
%```
% •

```{figure} /_static/infra/arquitectura.png
```

(infra-tabla)=
## Tabla comparativa

A continuación una tabla que compara las caracteristicas de cada cluster disponible:

|     | Mendieta | Mulatona | Eulogia | Serafín |
| --- | -------- | -------- | ------- | ------- |
| Uso recomendado        | Cómputo con GPUs | Trabajos pequeños | Sólo software preinstalado | Propósito general |
| Nodos de cómputo       | 18 | 7 | 32 | 60 |
| Cores/Threads por nodo | 20C/20T<br>(Xeon E5-2680 v2) | 32C/32T<br>(Xeon E5-2683 v4) | 64C/256T<br>(Xeon Phi 7210) | 64C/64T<br>(EPYC 7532) |
| GPUs por nodo          | 2 x Nvidia A30 | No | No | No |
| RAM por nodo           | 64GB | 128GB | 96GB | 256GB |
| [{abbr}`Almacenamiento local por nodo (Directorio /scratch)`](infra-scratch) | 400GB<br>SSD SATA | 95GB<br>SSD SATA | 200GB<br>SSD SATA | 1TB<br>SSD NVMe |
| Red interna             | 40Gbps<br>(Infiniband QDR) | 40Gbps<br>(Infiniband QDR) | 40Gbps<br>(Infiniband QDR) | 100Gbps<br>(Infiniband HDR100) |
| Trabajo más pequeño    | 1/2 nodo<br>(10 cores, 1 GPU) | 1 core | 1 nodo | 1 nodo |
| Compiladores           | GCC 11<br>NVIDIA HPC SDK 22.3 | GCC 12<br>Intel 2021<br>OneApi 2023 | GCC 12<br>Intel 2021 | GCC 12<br>AMD AOCC 4.1 |
| MPI                    | Open MPI 4.1 | Open MPI 4.1 | Open MPI 4.1 | Open MPI 4.1 |
| Codename               | ivb | bdw | knl | rome |
| Dirección              | `mendieta.ccad.unc.edu.ar` | `mulatona.ccad.unc.edu.ar` | `eulogia.ccad.unc.edu.ar` | `serafin.ccad.unc.edu.ar` |

(infra-slurm-colas)=
## Colas de Slurm

:::{margin}
Cada cluster tiene límites en las colas
Para elegir una cola, [agregar al script](slurm-script-cola):

```slurm
#SBATCH --partition=nombre
```

Para ver más info:
```console
$ sinfo
```
:::

:::{table} * Cola por defecto
|           | Mendieta | Mulatona | Eulogia | Serafín |
| --------- | -------- | -------- | ------- | ------- |
| **short** | *Hasta 1h    | *Hasta 1h, 1 nodo | Hasta 1h      | *Hasta 1h    |
| **multi** | Hasta 2 días | - | *Hasta 4 días | Hasta 2 días |
| **mono**  | -            | Hasta 2 días, 1 nodo | -             | -            |
:::

## Datacenters

```{figure} /_static/infra/infra.png
```
