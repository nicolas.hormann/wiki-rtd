---
html_theme.sidebar_secondary.remove:
og:title: Contactar a soporte
---

(soporte)=
# 🛟 Contactar a soporte

Antes de comunicarse con soporte, use el buscador para asegurarse que no está en la wiki lo que necesita.

Puede buscar o preguntar en el [Zulip](https://ccadunc.zulipchat.com/), otros usuarios le pueden ayudar.

Si nada de eso funciona, mandar un email a `soporte@ccad.unc.edu.ar` especificando claramente su duda o problema.

No se olvide de mencionar:
- Su usuario y el cluster en el que ocurre el problema.
- El directorio en donde estaba trabajando.
- El script de lanzamiento de SLURM que usó.
- El que archivo donde se encuentra la salida completa del error.


# Agendar una reunión

Si tiene una duda puntual,
puede agendar una reunión virtual (Google Meet) con el CPA Alejandro:

[https://calendly.com/ccad-soporte-ale/consulta](https://calendly.com/ccad-soporte-ale/consulta)
