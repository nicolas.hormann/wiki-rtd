# Configuration file for the Sphinx documentation builder.

# -- Project information

project = "wiki"

release = "1.0"
version = "1.0.0"

# -- General configuration

extensions = [
    "sphinx.ext.duration",
    "sphinx.ext.doctest",
    "myst_parser",
    "sphinx_copybutton",
    "sphinxcontrib.youtube",
    "sphinx_design",
    "sphinx_favicon",
    "sphinxext.opengraph",
    "notfound.extension",
    "sphinxcontrib.asciinema",
]

language = "es"

templates_path = ["_templates"]

copybutton_exclude = '.linenos, .gp, .go'

sphinx_tabs_disable_tab_closing = True

favicons = ["icon.svg"]

ogp_site_name = "UNC Supercómputo wiki"
ogp_enable_meta_description = True

# Tab color in Chrome, Android
ogp_custom_meta_tags = [
    '<meta name="theme-color" content="#6456FF">',
]

notfound_urls_prefix = ""
notfound_context = {
    "title": "404",
    "body": "<h1>Hmmm...</h1>Lo que sea que estés buscando, no es por acá. Intentá usando el <b>buscador</b>.",
}

# -- Options for HTML output

html_theme = "sphinx_book_theme"

html_static_path = ["_static"]
html_css_files = [
    "custom.css",
]
html_title = "CCAD"
html_last_updated_fmt = ""

html_theme_options = {
    "repository_url": "https://gitlab.com/ccad/wiki-rtd",
    "path_to_docs": "docs",
    "use_source_button": True,
    "use_edit_page_button": True,
    "use_repository_button": True,
    "use_fullscreen_button": False,
    # "announcement": "⚠️ Work In Progress! ⚠️",

    # Por ahora no funciona con Gitlab
    # https://github.com/executablebooks/jupyter-book/issues/1333
    #"use_issues_button": True,

    "content_footer_items": ["last-updated"],
    "extra_footer": "Centro de Computación de Alto Desempeño (CCAD)<br>Universidad Nacional de Córdoba (UNC)<br>UNC Supercómputo",

    "toc_title": "Contenido",
    "home_page_in_toc": True,

    "logo": {
        "image_light": "_static/logo-light.svg",
        "image_dark": "_static/logo-dark.svg",
        "alt_text": "CCAD web",
        "link": "https://ccad.unc.edu.ar",
   },
}

# -- Options for parsing Markdown

myst_enable_extensions = [
    "colon_fence",
    "fieldlist",
]

myst_heading_anchors = 3
